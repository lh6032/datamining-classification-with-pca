# -*- coding: utf-8 -*-
# @Time  : 2021/4/1 上午12:22
# @Author : Xinyu Zhang & Sophie Hou
# @FileName: Xinyu_Sophie_code.py
# @Software: PyCharm
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from numpy import linalg
from sklearn.linear_model import LogisticRegression
from numpy import mean, cov
from numpy.linalg import eig


def readfile(filepath):
    """
    Read file from filepath
    :param filepath: str
    :return: df
    """
    return pd.read_csv(filepath)


def data_pre(df, columns):
    """
    Delete column from df by given columns
    :param df: dataframe
    :param columns: [str]
    :return: cleaned dataframe
    """
    class_id = []
    for class_value in df.iloc[:, -1]:
        class_id.append(0 if class_value == "Assam" else 1)
    df['Class_id'] = class_id
    copy_data_id = df.iloc[:, -1]
    df_columns = df.columns
    for column in columns:
        if column in df_columns:
            df.drop(columns=column, inplace=True)
    return copy_data_id, df


def KLT_transformation(df):
    """
    Implemet KLT transformation and calculate eigenvalue and eigenvector
    :param df: cleaned data frame
    :return: eigenvalue and eigenvector
    """
    average_value = df.mean(axis=0)
    A = df.values
    M = mean(A.T, axis=1) # calculate the mean of each column
    C = A - M  # center columns by subtracting column means
    V = cov(C.T)  # calculate covariance matrix of centered matrix, THIS IS SAME 
    eig_values, eig_vectors = eig(V)
    print("vectores ", eig_vectors)
    print("values", eig_values)
    dict_eig = {}
    for i1, i2 in zip(eig_values, eig_vectors.T): # need transpose vectors to match egi_values
        dict_eig[round(i1, 2)] = [round(x, 2) for x in i2]
    print("dict_eig=", dict_eig)
    print("sorted dict=  ", dict(sorted(dict_eig.items(), reverse=True)))
    
    # PROJECT DATA 
    P = eig_vectors.T.dot(C.T)
    # print("PROJECT DATA ", P.T)

    for col_index in range(len(df.columns)):
        cur_col = df.iloc[:, col_index]
        cur_col -= average_value[col_index]
        df.iloc[:, col_index] = cur_col
    new_data = df
    covariance_matrix = df.cov()
    eigenvalue, eigenvector = linalg.eig(covariance_matrix)
    eigenvalue_arr, eigenvector_arr = [], []
    for col_index in range(len(df.columns)):
        eigenvalue_arr.append(eigenvalue[col_index])
        eigenvector_arr.append(eigenvector[:, col_index])
    eigenvalue_arr, eigenvector_arr = zip(*sorted(zip(eigenvalue_arr, eigenvector_arr), reverse=True))
    each_round = 1
    print(df.columns)
    for value, vector in zip(eigenvalue_arr, eigenvector_arr):
        print(f"========{each_round}=========")
        print(f"eigenvalue: {round(value, 2)}, eigenvector: {list(map(lambda x: round(x,2), vector))}")
        sorted_eigenvector_list = sorted(vector)
        first_vital, second_vital = list(vector).index(sorted_eigenvector_list[-1]), \
                                    list(vector).index(sorted_eigenvector_list[-2])
        print(f"Significance is {df.columns[first_vital]}, {df.columns[second_vital]}")
        each_round += 1
    return eigenvalue_arr, eigenvector_arr, new_data


def plot_eigenvectors(eigenvalue_arr):
    """
    Plot eigenvectors for Part I
    :param eigenvalue_arr: eigenvalue
    :return: None
    """
    eigenvalue_abs_arr = [abs(value) for value in eigenvalue_arr]
    total_eigenvalue = sum(eigenvalue_abs_arr)
    divided_eigenvalue_arr = [value / total_eigenvalue for value in eigenvalue_abs_arr]
    sorted_eigenvalue_arr = [sum(divided_eigenvalue_arr[: column_index]) for column_index in range(len(df.columns) + 1)]
    print('sorted_eigenvalue_arr = ', sorted_eigenvalue_arr)
    print(f"The number of eigenvectors that need to “capture” 95% of the variance is: {sum(value <= 0.95 for value in sorted_eigenvalue_arr)}")
    plt.plot(sorted_eigenvalue_arr, "--bo")
    plt.yticks(np.arange(0, 1.2, 0.1))
    plt.xlim(0, len(df.columns))
    plt.ylim(0, 1)
    plt.grid()
    plt.xlabel("Number of Eigenvectors")
    plt.ylabel("Cumulative Amount of Variance Captured")
    plt.show()


def part_I(df):
    """
    Present the result of Part I
    :param df: data frame
    :return: None
    """
    columns = ['Record ID', 'Class_id', 'Class']
    data_id, cleaned_df = data_pre(df, columns)
    eigenvalue_arr, _, _ = KLT_transformation(cleaned_df)
    plot_eigenvectors(eigenvalue_arr)


def part_ii_test(df):
    # average_value = df.mean(axis=0)
    columns = ['Record ID', 'Class_id', 'Class']
    data_id, df = data_pre(df, columns)
    A = df.values
    M = mean(A.T, axis=1) # calculate the mean of each column
    C = A - M  # center columns by subtracting column means
    V = cov(C.T)  # calculate covariance matrix of centered matrix, THIS IS SAME 
    eig_values, eig_vectors = eig(V)
    print("vectores ", eig_vectors)
    print("values", eig_values)
    dict_eig = {}
    for i1, i2 in zip(eig_values, eig_vectors.T): # need transpose vectors to match egi_values
        dict_eig[round(i1, 2)] = [round(x, 2) for x in i2]
    # print("dict_eig=", dict_eig)
    # print("sorted dict=  ", dict(sorted(dict_eig.items(), reverse=True)))
    # P = eig_vectors.T.dot(C.T)
    df["Class"] = data_id
    best_score = 0
    best_pair_A, best_pair_B = 0, 0
    for vec_id_A in range(0, len(eig_vectors) - 1):
        for vec_id_B in range(vec_id_A + 1, len(eig_vectors)):
            vector_A = eig_vectors.T[vec_id_A]
            print('vector_A=', vector_A)
            vector_B = eig_vectors.T[vec_id_B]
            print('vector_B=', vector_B)
            # print(C)
            x_values = np.dot(C, vector_A)  # pca A
            print("X SIZE: ", x_values.shape)

            y_values = np.dot(C, vector_B)   # PCA B
            print("Y SIZE: ", y_values.shape)

            X = [(x, y) for x, y in zip(x_values, y_values)]
            # print('X=', X)
            print("X SIZE: ", len(X), len(X[0]))
            Assam_index = df.loc[df.Class == 0].index.values   # GET INDEX OF CLASS A
            print('Assam_index= ', Assam_index)
            Bhuttan_index = df.loc[df.Class == 1].index.values # GET INDEX OF CLASS B
            print('Bhuttan_index=',Bhuttan_index)
            Assam_x = [x_values[index] for index in Assam_index]
            print(Assam_x[:10])
            Assam_y = [y_values[index] for index in Assam_index]
            Bhuttan_x = [x_values[index] for index in Bhuttan_index]
            Bhuttan_y = [y_values[index] for index in Bhuttan_index]
            # plt.figure(figsize=(12,10))
            # plt.scatter(Assam_x, Assam_y, marker="o", c="none", edgecolors="r", label="Class Assam", alpha=0.5)
            # plt.scatter(Bhuttan_x, Bhuttan_y, marker="+", c="b", label="Class Bhuttan", alpha=0.5)
            # plt.xlabel(f"Amount of PCA {vec_id_A + 1}")
            # plt.ylabel(f"Amount of PCA {vec_id_B + 1}")
            # plt.title(f"Data Projected Onto Eigenvectors {vec_id_A + 1} and {vec_id_B + 1}")
            # plt.legend(loc='upper left')
            # # plt.savefig(f'/Users/sophiehou/Desktop/720/Result2/Figure_{vec_id_A + 1}_{vec_id_B + 1}.jpg')
            # plt.show()
    print(f"The best two eigenvectors to use is: {best_pair_A}, {best_pair_B}, with score of {round(best_score, 2)}")

    # PROJECT DATA 
    # P = eig_vectors.T.dot(C.T)

def part_II(df):
    """
    Present the result of Part II
    :param df: data frame
    :return: None
    """
    columns = ['Record ID', 'Class_id', 'Class']
    data_id, cleaned_df = data_pre(df, columns)
    eigenvalue_arr, eigenvector_arr, new_df = KLT_transformation(cleaned_df)
    new_data = new_df.to_numpy()
    df["Class"] = data_id
    best_score = 0
    best_pair_A, best_pair_B = 0, 0
    for vec_id_A in range(0, len(eigenvector_arr) - 1):
        for vec_id_B in range(vec_id_A + 1, len(eigenvector_arr)):
            vector_A = eigenvector_arr[vec_id_A]
            vector_B = eigenvector_arr[vec_id_B]
            x_values = np.dot(new_data, vector_A)
            y_values = np.dot(new_data, vector_B)
            X = [(x, y) for x, y in zip(x_values, y_values)]
            model = LogisticRegression()
            model.fit(X, df.Class)
            score = model.score(X,df.Class)
            if score >= best_score:
                best_score = score
                best_pair_A, best_pair_B = vec_id_A + 1, vec_id_B + 1
            Assam_index = df.loc[df.Class == 0].index.values
            Bhuttan_index = df.loc[df.Class == 1].index.values
            Assam_x = [x_values[index] for index in Assam_index]
            Assam_y = [y_values[index] for index in Assam_index]
            Bhuttan_x = [x_values[index] for index in Bhuttan_index]
            Bhuttan_y = [y_values[index] for index in Bhuttan_index]
            plt.figure(figsize=(12,10))
            plt.scatter(Assam_x, Assam_y, marker="o", c="none", edgecolors="r", label="Class Assam", alpha=0.5)
            plt.scatter(Bhuttan_x, Bhuttan_y, marker="+", c="b", label="Class Bhuttan", alpha=0.5)
            plt.xlabel(f"Amount of PCA {vec_id_A + 1}")
            plt.ylabel(f"Amount of PCA {vec_id_B + 1}")
            plt.title(f"Data Projected Onto Eigenvectors {vec_id_A + 1} and {vec_id_B + 1}")
            plt.legend(loc='upper left')
            # plt.savefig(f'/Users/sophiehou/Desktop/720/Result2/Figure_{vec_id_A + 1}_{vec_id_B + 1}.jpg')
            plt.show()
    print(f"The best two eigenvectors to use is: {best_pair_A}, {best_pair_B}, with score of {round(best_score, 2)}")


if __name__ == '__main__':
    df = readfile("../Xinyu_Sophie/Abominable_Training.csv")
    print(df.head(20))
    # part_I(df)
    part_II(df)
    # part_ii_test(df)
